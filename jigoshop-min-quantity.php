<?php
	/*
		Plugin Name: Jigoshop Minimum Quantity for Cart
		Version: 0.1
		Description: Force a minimum quantity for any Jigoshop cart item 
		Author: Daniel Rabb
		Author URI: http://danielrabb.com/
		Plugin URI: http://danielrabb.com/jigoshop_plugin/
		License: GPL2
	*/

	include_once(ABSPATH .'wp-admin/includes/plugin.php');

	global $wp_version;

	$min_ver_msg = 'Jigoshop Minimum Quantity plugin requires WordPress 2.5 or higher.';
	$jigoshop_req_msg = 'Jigoshop eCommerce plugin is required for this plugin.';

	if (version_compare($wp_version, "2.5", "<")) {
		exit($exit_msg);
	}

	if (!is_plugin_active('jigoshop/jigoshop.php')) {
		exit($jigoshop_req_msg);
	}

	add_action('wp_footer', 'min_quantity_javascript');  // setup the hooks used throughout
	add_action('init', 'jigoshop_add_to_cart_action_min_quant');
	add_action('add_meta_boxes', 'min_quantity_meta_box_add');
	add_action('save_post', 'min_quantity_save_data', 1, 2);

	function min_quantity_meta_box_add() {
		add_meta_box('my_meta_box_id', 'Jigoshop Minimum Quantity', 'min_quantity_meta_box_add_cb', 'product', 'normal', 'high');
	}

	function min_quantity_meta_box_add_cb() {
		global $post;
		
		wp_nonce_field(basename (__FILE__), 'metabox_noncename');
		$min_quantity = get_post_meta($post->ID, '_jigoshop_min_quantity', true);

		echo "Minimum Cart Quantity: <input name=\"_jigoshop_min_quantity\" type=\"text\" size=\"15\" value=\"". $min_quantity ."\" />";

		$prod = new jigoshop_product($post->ID);
		if ( $prod->get_children() ) {

			//foreach (jigoshop_cart::$cart_contents as $cart_item_key => $values)

			foreach ( $prod->get_children() as $variation ) {
				$varPost = get_post($variation);

				if ($varPost->post_type == "product_variation") {
					$varMeta = get_post_meta($variation);

					echo "<br />var data: ". $varMeta->variation_data[0];
				}
				//echo "<br />var id: ". $variation->post_type;
			}

			//echo "<b>This product has children</b>";
		}
	}

	function min_quantity_save_data($post_ID, $post) {
		if (!isset($_POST['metabox_noncename']) || !wp_verify_nonce($_POST['metabox_noncename'], basename(__FILE__)))  // ensure nonce is set
			return $post_ID;

		$post_type = get_post_type_object($post->post_type);

		if (!current_user_can($post_type->cap->edit_post, $post_ID))  // check login user can edit post
			return $post_ID;

		$meta_key = '_jigoshop_min_quantity';

		$min_quantity = mysql_real_escape_string($_POST[$meta_key]);  // grab & sanitize user input
		$old_min_quantity = get_post_meta($post_ID, $meta_key, true);

		if ($min_quantity && $old_min_quantity == '') {  // have new value & no old value = add
			add_post_meta($post->ID, $meta_key, $min_quantity);
		} elseif ($min_quantity && $min_quantity != $old_min_quantity) {  // have old value, and it isn't new value
			update_post_meta($post->ID, $meta_key, $min_quantity);
		} elseif ($min_quantity == '' && $old_min_quantity) {  // no new value, delete existing
			delete_post_meta($post_ID, $meta_key, $old_min_quantity);
		}
	}

	$minQuantityWasSet = false;
	$newQuantity = 0;

	function jigoshop_add_to_cart_action_min_quant() {
		global $minQuantityWasSet, $newQuantity;

		foreach (jigoshop_cart::$cart_contents as $cart_item_key => $values) {
		
			$_product = $values['data'];

			if ($_product->exists() && $values['quantity'] > 0) { // make sure this item has quantity
				$prod_id = ($_product->ID)?$_product->ID:'';

				$cart_quantity  = $values['quantity'];
				$prod_min_quantity = get_post_meta($prod_id, "_jigoshop_min_quantity", true);  // see if we have a min quantity store for this product

				$min_quantity = ($prod_min_quantity) ? $prod_min_quantity : 0;

				if ($min_quantity > $cart_quantity) {
					jigoshop_cart::set_quantity($cart_item_key, $min_quantity);  // set quantity to minimum if cart doesn't have minimum
					$minQuantityWasSet = true;  // flag later for UI message
					$newQuantity = $min_quantity;
				}
			}
		}
	}

	function min_quantity_javascript() {
		global $minQuantityWasSet, $newQuantity;

	 	if ($minQuantityWasSet) {
	 		$cartMessage = "<br />The minimum quantity for this product is ". $newQuantity .".&nbsp;&nbsp;". $newQuantity ." items have been added to your cart.";
	 		$minQuantityWasSet = false;
	 		$newQuantity = 0; ?>

			<script type='text/javascript' language='javascript'> 
				jQuery(document).ready(function(){ 
					jQuery('div.jigoshop_message').append('<?php echo $cartMessage; ?>');
				});
			</script>
		
		<?php }
	}
?>